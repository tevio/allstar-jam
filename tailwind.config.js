module.exports = {
  theme: {
    extend: {
      inset: {
        '75per': '50%',
        '50per': '50%',
        '25per': '25%'
      }
    }
  },
  variants: {},
  plugins: []
}
